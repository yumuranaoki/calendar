class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :edit, :update]
  before_action :correct_user,   only: [:edit, :update]
  
  def new
    @user = User.new
  end
  
  def index
    @searched_users = User.search(params[:search])
    @users = @searched_users.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    if !@user.google_calendars.empty?
      google_calendar = current_user.google_calendars.order(:id).first
      refresh_token = google_calendar.refresh_token
      client = Signet::OAuth2::Client.new(client_options)
      client.update!(refresh_token: refresh_token)
      response = client.fetch_access_token!
      session[:authorization] = response
      service = Google::Apis::CalendarV3::CalendarService.new
      service.authorization = client
      if google_calendar.sync == false
        redirect_to list_part_url
      elsif google_calendar.sync == true
        google_calendar.update(sync: false)
      end
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      UserMailer.account_activation(@user).deliver_now
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def destroy
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def following
    @user = User.find(params[:id])
    @users = @user.user_followeds
  end
  
  def followers
    @user = User.find(params[:id])
    @users = @user.user_followers
  end
  
  
  private

    def user_params
      params.require(:user).permit(:name, :email,
                                   :password, :password_confirmation)
    end
    
    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end
    
    def client_options
        {          
        client_id: Rails.application.secrets.google_client_id,
        client_secret: Rails.application.secrets.google_client_secret,
        authorization_uri: 'https://accounts.google.com/o/oauth2/auth',
        token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
        scope: Google::Apis::CalendarV3::AUTH_CALENDAR,
        redirect_uri: callback_url
        }
    end
  
end
