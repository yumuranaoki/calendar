class GroupsController < ApplicationController
  def index
    @user = User.find(params[:user_id])
    @groups = @user.groups
  end

  #記録用が多いので後で消す
  def show
    @users = current_user.user_followeds.search(params[:search])
    @user = User.find(params[:user_id])
    @group = @user.groups.find(params[:id])
    @event_count = 0
    @convinient_event = []
    @inconvinient_event = []
    @follower_count = 0
    @time_count = 0
    @follower_count_array = []
    @convinient_time_array = []
    @time_number = ((@group.endtime - @group.starttime) / 3600 ).floor - 1
    @eventforward = []
    @eventback = []
    
    for num_day in 0..6
    for num in Range.new(24*num_day,24*num_day + @time_number)
      @group.followers.each do |follower|
        if follower.events.any?
          follower.events.each do |event|
            num_next = num + 1
            if event.end < @group.starttime.since(num.hour) || @group.starttime.since(num_next.hour) < event.start
              @event_count += 1
              @convinient_event.push(event.title)
              @eventforward.push(event.title)
              @eventforward.push(event.start)
              @eventforward.push(event.end)
              @eventforward.push(@group.starttime.since(num.hour))
            else
              @inconvinient_event.push(event.title)
            end
          end
          if @event_count == follower.events.count
            @follower_count += 1
          end
          @event_count = 0
        else
          @follower_count += 1
        end
      end
      if @follower_count == @group.followers.count
        @convinient_time_array.push(num)
      else
        @time_count += 1
      end
      @follower_count = 0
    end
    end
    
    
    
    i = @convinient_time_array[0]
    @complete_array = @convinient_time_array.inject([@convinient_time_array[0]]) do |r, v|
      if v != i
        if i - r.last >= 2
         r << "to #{i - 1}"
        end
        r << v
        i = v
      end
      i += 1
      r
    end
    
    @completed_time_array = []
    @position_start = 0
    @position_end = 0
    @number_start = 0
    @number_end = 0
    
  end
  
  def participating
    @groups = current_user.followeds
  end
  
  def invited
    @groups = current_user.followers
  end

  def new
    @group = current_user.groups.build
  end

  def create
    @group = current_user.groups.build(group_params)
    if @group.save
      current_user.follow(@group)
    end
    respond_to do |format|
      if @group.save
        logger.debug("save成功")
        format.js { @status = 'success' }
      else
        logger.debug("save失敗")
        format.js { @status = 'fail' }
      end
    end
  end

  def destroy
  end
  
    private
      
      def group_params
        params.require(:group).permit(:title, :content, :starttime, :endtime, :timelength)
      end
      
      
end
